using System;
using System.Linq;
using NYoutubeDL.Models;

namespace Yodel.Models
{
    public class DownloadInfoModel
    {
        public string Id { get; }
        public string Thumbnail { get; }
        public string Title { get; }
        public string Size { get; }
        public int Progress { get; }
        public string Eta { get; }
        public string Rate { get; }

        public DownloadInfoModel(VideoDownloadInfo videoInfo)
        {
            try
            {
                if (videoInfo == null) return;

                this.Id = videoInfo.Id;
                this.Thumbnail = videoInfo.Thumbnail ?? videoInfo.Thumbnails?.FirstOrDefault().Url ?? string.Empty;
                this.Title = videoInfo.Title;
                this.Size = videoInfo.VideoSize ?? string.Empty;
                this.Progress = videoInfo.VideoProgress;
                this.Eta = videoInfo.Eta;
                this.Rate = videoInfo.DownloadRate;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}