using System.IO;
using System.Threading.Tasks;
using Yodel.Utilities;

namespace Yodel.Interop
{
    public class FfmpegInterop
    {
        public bool OnPath()
        {
            return Ffmpeg.OnPath || File.Exists(Constants.FfmpegDismissedFile);
        }

        public async Task Download()
        {
            await Ffmpeg.Download();
        }

        public void Dismiss()
        {
            Ffmpeg.Dismiss();
        }
    }
}