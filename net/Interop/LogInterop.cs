using Serilog;

namespace Yodel.Interop
{
    public class LogInterop
    {
        public void Verbose(string message)
        {
            Log.Verbose(message);
        }

        public void Information(string message)
        {
            Log.Information(message);
        }

        public void Debug(string message)
        {
            Log.Debug(message);
        }

        public void Error(string message)
        {
            Log.Error(message);
        }

        public void Fatal(string message)
        {
            Log.Fatal(message);
        }
    }
}