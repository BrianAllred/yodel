using System;
using Yodel.Utilities;

namespace Yodel.Interop
{
    public class PlatformInterop
    {
        public bool IsWindows => Platform.IsWindows;

        public bool IsOSX => Platform.IsOSX;

        public bool IsLinux => Platform.IsLinux;

        public void ExitApplication()
        {
            Environment.Exit(0);
        }

        public string OpenFolderDialog(string directory)
        {
            return Platform.OpenFolderDialog(directory);
        }

        public string GetDefaultOutput()
        {
            return Platform.DefaultOutputPath;
        }
    }
}