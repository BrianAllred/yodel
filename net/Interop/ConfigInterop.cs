using System;
using System.IO;
using Serilog;
using Yodel.Utilities;
using Options = NYoutubeDL.Options.Options;

namespace Yodel.Interop
{
    public class ConfigInterop
    {
        private readonly object lockObject = new object();

        private Options options;

        public ConfigInterop()
        {
            AppDomain.CurrentDomain.ProcessExit += (sender, args) =>
            {
                try
                {
                    this.SaveConfig(options);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error saving config.");
                }
            };
        }

        public Options LoadConfig()
        {
            lock (lockObject)
            {
                try
                {
                    Log.Verbose("Loading config file.");
                    if (!File.Exists(Constants.ConfigFile))
                    {
                        Log.Debug("No config file present.");
                        return new Options();
                    }

                    return Options.Deserialize(File.ReadAllText(Constants.ConfigFile));
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error loading config file at {ConfigFilepath}", Constants.ConfigFile);
                    return new Options();
                }
            }
        }

        public void SaveConfig(Options options)
        {
            if (options != null)
            {
                lock (lockObject)
                {
                    try
                    {
                        Log.Information("Saving config file.");
                        File.WriteAllText(Constants.ConfigFile, options.Serialize());
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Error saving config file at {ConfigFilepath}", Constants.ConfigFile);
                    }
                }
            }
        }

        public void SyncConfig(Options options)
        {
            lock (lockObject)
            {
                try
                {
                    Log.Verbose("Syncing config file.");
                    this.options = options;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error syncing config file");
                }
            }
        }
    }
}