using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using NYoutubeDL;
using NYoutubeDL.Models;
using Qml.Net;
using Serilog;
using Yodel.Models;
using Yodel.Utilities;
using Options = NYoutubeDL.Options.Options;

namespace Yodel.Interop
{
    [Signal("videoUpdateSignal", NetVariantType.Object)]
    public class YdlInterop
    {
        private readonly object infoLock = new object();

        private DownloadInfo info;

        private readonly Dictionary<string, VideoDownloadInfo> videos = new Dictionary<string, VideoDownloadInfo>();

        public Options NewOptions()
        {
            return new Options();
        }

        public YoutubeDL NewClient()
        {
            return new YoutubeDL();
        }

        public async Task Download(YoutubeDL client)
        {
            var destructuredClient = new { ClientUrl = client.VideoUrl, Options = client.Options };
            Log.Information("Performing download: {@Client}", destructuredClient);

            try
            {

                var task = client.DownloadAsync();

                void SetInfo(object sender, PropertyChangedEventArgs args)
                {
                    client.InfoChangedEvent -= SetInfo;
                    this.SetDownloadInfo((DownloadInfo)sender);
                }

                client.InfoChangedEvent += SetInfo;
                await task;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error performing download.");
            }
        }

        public async Task Update()
        {
            Log.Information("Performing internal update of youtube-dl.");

            try
            {
                var ydlClient = new YoutubeDL();
                ydlClient.Options.GeneralOptions.Update = true;
                await ydlClient.DownloadAsync();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error updating youtube-dl.");
            }
        }

        public bool OnPath()
        {
            return Ydl.OnPath;
        }

        public bool PythonOnPath()
        {
            return Ydl.PythonOnPath;
        }

        public async Task DownloadYdl()
        {
            await Ydl.Download();
        }

        private void SetDownloadInfo(DownloadInfo info)
        {
            this.info = info;
            if (this.info is VideoDownloadInfo)
            {
                this.info.PropertyChanged += this.VideoDownloadChanged;
                this.VideoDownloadChanged(this.info, null);
            }
            else if (this.info is PlaylistDownloadInfo playlistDownloadInfo)
            {
                foreach (VideoDownloadInfo video in playlistDownloadInfo.Videos)
                {
                    video.PropertyChanged += this.VideoDownloadChanged;
                    this.VideoDownloadChanged(video, null);
                }
            }
            else if (this.info is MultiDownloadInfo multiDownloadInfo)
            {
                foreach (VideoDownloadInfo video in multiDownloadInfo.Videos)
                {
                    video.PropertyChanged += this.VideoDownloadChanged;
                    this.VideoDownloadChanged(video, null);
                }

                foreach (PlaylistDownloadInfo playlist in multiDownloadInfo.Playlists)
                {
                    foreach (VideoDownloadInfo plVideo in playlist.Videos)
                    {
                        plVideo.PropertyChanged += this.VideoDownloadChanged;
                        this.VideoDownloadChanged(plVideo, null);
                    }
                }
            }
        }

        private void VideoDownloadChanged(object sender, PropertyChangedEventArgs e)
        {
            lock (this.infoLock)
            {
                VideoDownloadInfo videoInfo = (VideoDownloadInfo)sender;

                if (!string.IsNullOrEmpty(videoInfo.Id) && !string.IsNullOrEmpty(videoInfo.Title))
                {
                    this.videos[videoInfo.Id] = videoInfo;
                    this.ActivateVideoUpdateSignal(new DownloadInfoModel(videoInfo));
                }
            }
        }

        private void ActivateVideoUpdateSignal(DownloadInfoModel videoInfo)
        {
            if (QCoreApplication.IsMainThread)
            {
                this.ActivateSignal("videoUpdateSignal", videoInfo);
            }
            else
            {
                QtDispatcher.Instance.QueueDispatch(new Action(() => this.ActivateSignal("videoUpdateSignal", videoInfo)));
            }
        }
    }
}