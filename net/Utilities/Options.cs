using CommandLine;

namespace Yodel.Utilities
{
    public class Options
    {

        [Option('v', "verbose", Required = false, HelpText = "Set logging to verbose mode.")]
        public bool Verbose { get; set; }
    }
}