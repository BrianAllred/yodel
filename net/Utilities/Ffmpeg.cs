using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Serilog;

namespace Yodel.Utilities
{
    public static class Ffmpeg
    {
        private const string win32Url = "https://ffmpeg.zeranoe.com/builds/win32/static/ffmpeg-latest-win32-static.zip";
        private const string win64Url = "https://ffmpeg.zeranoe.com/builds/win64/static/ffmpeg-latest-win64-static.zip";
        private const string osx64Url = "https://ffmpeg.zeranoe.com/builds/macos64/static/ffmpeg-latest-macos64-static.zip";
        private const string linuxUrlTemplate = "https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-$(arch)-static.tar.xz";

        internal static bool OnPath => "ffmpeg".OnPath();

        internal static async Task Download()
        {
            // Set the directory so we download assets to the right place.
            var currentDirectory = Environment.CurrentDirectory;
            Directory.SetCurrentDirectory(Constants.LocalAppDataFolder);

            if (Platform.IsWindows)
            {
                await WindowsDownload();
            }
            else if (Platform.IsOSX)
            {
                await OSXDownload();
            }
            else if (Platform.IsLinux)
            {
                await LinuxDownload();
            }

            Directory.SetCurrentDirectory(currentDirectory);
        }

        internal static void Dismiss()
        {
            File.Create(Constants.FfmpegDismissedFile);
        }

        private static async Task LinuxDownload()
        {
            string arch = "";
            switch (Platform.Architecture)
            {
                case Architecture.X86:
                    arch = "i686";
                    break;
                case Architecture.X64:
                    arch = "amd64";
                    break;
                case Architecture.Arm:
                    arch = "armel";
                    break;
                case Architecture.Arm64:
                    arch = "arm64";
                    break;
            }

            string url = linuxUrlTemplate.Replace("$(arch)", arch);

            Log.Information("Downloading Linux version of FFMpeg.");
            Log.Debug("Download url is {URL}", url);

            try
            {
                using (var client = new WebClient())
                {
                    await client.DownloadFileTaskAsync(url, "ffmpeg.tar.xz");
                    Directory.CreateDirectory("ffmpeg-static");
                    await Command.RunAsync("tar", "-xf", "ffmpeg.tar.xz", "--strip=1", "-C ./ffmpeg-static");
                    File.Move("ffmpeg-static/ffmpeg", "ffmpeg");
                    File.Move("ffmpeg-static/ffprobe", "ffprobe");
                    Directory.Delete("ffmpeg-static", true);
                    File.Delete("ffmpeg.tar.xz");

                    Command.Run("chmod", "+x", "ffmpeg");
                    Command.Run("chmod", "+x", "ffprobe");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error downloading FFMpeg.");
            }
        }

        private static async Task WindowsDownload()
        {
            var url = Platform.Architecture == Architecture.X86 ? win32Url : win64Url;

            Log.Information("Downloading Windows version of FFMpeg.");
            Log.Debug("Download url is {URL}", url);

            try
            {
                using (var client = new WebClient())
                {
                    await client.DownloadFileTaskAsync(url, "ffmpeg.zip");
                    ZipFile.ExtractToDirectory("ffmpeg.zip", "ffmpeg");
                    foreach (var ffFile in Directory.GetFiles(string.Join(Path.DirectorySeparatorChar, "ffmpeg", "ffmpeg-latest-win64-static", "bin")))
                    {
                        File.Move(ffFile, Path.GetFileName(ffFile));
                    }

                    Directory.Delete("ffmpeg", true);
                    File.Delete("ffmpeg.zip");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error downloading FFMpeg.");
            }
        }

        private static async Task OSXDownload()
        {
            var url = osx64Url;

            Log.Information("Downloading OSX version of FFMpeg.");
            Log.Debug("Download url is {URL}", url);

            try
            {
                using (var client = new WebClient())
                {
                    await client.DownloadFileTaskAsync(url, "ffmpeg.zip");
                    ZipFile.ExtractToDirectory("ffmpeg.zip", "ffmpeg-static");
                    foreach (var ffFile in Directory.GetFiles(string.Join(Path.DirectorySeparatorChar, "ffmpeg-static", "ffmpeg-latest-macos64-static", "bin")))
                    {
                        File.Move(ffFile, Path.GetFileName(ffFile));
                    }

                    Directory.Delete("ffmpeg-static", true);
                    File.Delete("ffmpeg.zip");

                    Command.Run("chmod", "+x", "ffmpeg");
                    Command.Run("chmod", "+x", "ffprobe");
                    Command.Run("chmod", "+x", "ffplay");
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error downloading FFMpeg.");
            }
        }
    }
}