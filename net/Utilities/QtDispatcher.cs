using System;
using System.Collections.Concurrent;
using System.Threading;
using Qml.Net;

namespace Yodel.Utilities
{
    public sealed class QtDispatcher
    {
        private static readonly QtDispatcher instance = new QtDispatcher();

        private QCoreApplication application;

        private Timer dispatchTimer;

        private ConcurrentBag<Action> dispatchQueue = new ConcurrentBag<Action>();

        static QtDispatcher() { }

        private QtDispatcher()
        {
            dispatchTimer = new Timer(new TimerCallback(TimerTask), null, 0, 250);
        }

        public static QtDispatcher Instance { get { return instance; } }

        public void SetApplication(QCoreApplication application)
        {
            this.application = application;
        }

        public void QueueDispatch(Action action)
        {
            while (dispatchQueue.Count > 1)
            {
                dispatchQueue.TryTake(out _);
            }

            dispatchQueue.Add(action);
        }

        private void TimerTask(object state)
        {
            Action action;
            if (dispatchQueue.Count > 0 && dispatchQueue.TryTake(out action))
            {
                this.application.Dispatch(action);
            }
        }
    }
}