using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace Yodel.Utilities
{
    public static class Extensions
    {
        /// <summary>
        ///     Attempts to resolve the path of a given file info into a fully absolute path
        /// </summary>
        /// <param name="fileInfo">
        ///     File with relative path
        /// </param>
        /// <returns>
        ///     File's absolute path
        /// </returns>
        internal static string GetFullPath(this FileInfo fileInfo)
        {
            if (File.Exists(fileInfo.Name))
            {
                return Path.GetFullPath(fileInfo.Name);
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                string filePath = fileInfo.Name + ".exe";
                if (File.Exists(filePath))
                {
                    return Path.GetFullPath(filePath);
                }
            }

            string environmentVariable = Environment.GetEnvironmentVariable("PATH");
            if (environmentVariable != null)
            {
                foreach (string path in environmentVariable.Split(Path.PathSeparator))
                {
                    string fullPath = Path.Combine(path, fileInfo.Name);
                    if (File.Exists(fullPath))
                    {
                        return fullPath;
                    }

                    if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    {
                        fullPath += ".exe";
                        if (File.Exists(fullPath))
                        {
                            return fullPath;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Waits asynchronously for the process to exit.
        /// </summary>
        /// <param name="process">The process to wait for cancellation.</param>
        /// <param name="cancellationToken">A cancellation token. If invoked, the task will return 
        /// immediately as canceled.</param>
        /// <returns>A Task representing waiting for the process to end.</returns>
        internal static async Task WaitForExitAsync(this Process process, CancellationToken cancellationToken)
        {
            while (!process.HasExited)
            {
                await Task.Delay(100, cancellationToken);
            }
        }

        internal static bool OnPath(this string file)
        {
            return string.IsNullOrEmpty(file) || OnPath(new FileInfo(file));
        }

        internal static bool OnPath(this FileInfo file)
        {
            return !string.IsNullOrEmpty(Extensions.GetFullPath(file));
        }
    }
}