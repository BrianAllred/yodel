using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Yodel.Utilities
{
    public static class Command
    {
        internal static void Run(string command, params string[] args)
        {
            var process = BuildProcess(command, args);
            process.Start();
            process.WaitForExit();
        }

        internal static async Task RunAsync(string command, params string[] args)
        {
            var process = BuildProcess(command, args);
            process.Start();
            await process.WaitForExitAsync(new CancellationToken());
        }

        private static Process BuildProcess(string command, params string[] args)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = command,
                    Arguments = string.Join(' ', args),
                    CreateNoWindow = true,
                    UseShellExecute = true
                }
            };

            return process;
        }
    }
}