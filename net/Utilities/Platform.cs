using System;
using System.IO;
using System.Runtime.InteropServices;
using Eto.Forms;

namespace Yodel.Utilities
{
    public static class Platform
    {
        public static bool IsWindows => RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        public static bool IsOSX => RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

        public static bool IsLinux => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

        public static OSPlatform OS
        {
            get
            {
                if (Platform.IsLinux) return OSPlatform.Linux;
                if (Platform.IsWindows) return OSPlatform.Windows;
                if (Platform.IsOSX) return OSPlatform.OSX;
                return OSPlatform.FreeBSD;
            }
        }

        public static Architecture Architecture => RuntimeInformation.ProcessArchitecture;

        public static string DefaultOutputPath
        {
            get
            {
                var folder = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
                if (string.IsNullOrEmpty(folder))
                {
                    folder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                }

                return Path.Combine(folder, "%(title)s-%(id)s.%(ext)s");
            }
        }

        public static string AppDataFolder
        {
            get
            {
                var folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                if (string.IsNullOrEmpty(folder))
                {
                    if (IsWindows)
                    {
                        folder = Environment.GetEnvironmentVariable("appdata");

                        return folder ?? @"C:\";
                    }

                    folder = Environment.GetEnvironmentVariable("XDG_CONFIG_HOME");

                    return folder ?? "~/.config";
                }

                return folder;
            }
        }

        public static string LocalAppDataFolder
        {
            get
            {
                var folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                if (string.IsNullOrEmpty(folder))
                {
                    if (IsWindows)
                    {
                        folder = Environment.GetEnvironmentVariable("localappdata");

                        return folder ?? @"C:\";
                    }

                    folder = Environment.GetEnvironmentVariable("XDG_DATA_HOME");

                    return folder ?? "~/.local/share";
                }

                return folder;
            }
        }

        public static string OpenFolderDialog(string directory)
        {
            using (var dialog = new SelectFolderDialog())
            {
                dialog.Directory = directory;
                dialog.Title = "Select download location";
                if (dialog.ShowDialog(new Form()) != DialogResult.Cancel)
                {
                    return dialog.Directory;
                }
            }

            return directory;
        }
    }
}