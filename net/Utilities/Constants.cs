using System.IO;

namespace Yodel.Utilities
{
    public static class Constants
    {
        public static string AppDataFolder => Path.Combine(Platform.AppDataFolder, "Yodel");

        public static string LocalAppDataFolder => Path.Combine(Platform.LocalAppDataFolder, "Yodel");

        public static string FfmpegDismissedFile => Path.Combine(AppDataFolder, "ffmpegDismissed");

        public static string LogTextFile => Path.Combine(AppDataFolder, "log.txt");

        public static string LogJsonFile => Path.Combine(AppDataFolder, "log.json");

        public static string ConfigFile => Path.Combine(LocalAppDataFolder, "config.json");

        public const string LogTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}{NewLine}{Properties:j}";
    }
}