using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Serilog;

namespace Yodel.Utilities
{
    public static class Ydl
    {
        private const string winUrl = "https://github.com/ytdl-org/youtube-dl/releases/latest/download/youtube-dl.exe";

        private const string pythonUrl = "https://github.com/ytdl-org/youtube-dl/releases/latest/download/youtube-dl";

        internal static bool OnPath => "youtube-dl".OnPath();

        internal static bool PythonOnPath => "python".OnPath();

        internal static async Task Download()
        {
            Log.Information("Downloading youtube-dl binary");

            try
            {
                var url = Platform.IsWindows ? winUrl : pythonUrl;
                var filename = Platform.IsWindows ? Path.Combine(Constants.LocalAppDataFolder, "youtube-dl.exe") : Path.Combine(Constants.LocalAppDataFolder, "youtube-dl");

                Log.Debug("Download url is {URL}.", url);
                Log.Debug("Download filename is {Filename}", filename);

                using (var client = new WebClient())
                {
                    await client.DownloadFileTaskAsync(url, filename);

                    if (!Platform.IsWindows)
                    {
                        Command.Run("chmod", "+x", filename);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error downloading youtube-dl binary.");
            }
        }
    }
}