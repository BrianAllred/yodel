﻿using System;
using System.IO;
using System.Reflection;
using CommandLine;
using Qml.Net;
using Qml.Net.Runtimes;
using Serilog;
using Serilog.Exceptions;
using Serilog.Formatting.Json;
using Yodel.Interop;
using Yodel.Utilities;

namespace Yodel
{
    public class Program
    {

#if WINDOWS
        [STAThread]
#endif
        public static int Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(options =>
                {
                    LoggerConfiguration logConfig = new LoggerConfiguration()
                                                    .Enrich.WithThreadId()
                                                    .Enrich.WithProcessId()
                                                    .Enrich.WithMachineName()
                                                    .Enrich.WithExceptionDetails()
                                                    .WriteTo.Console(outputTemplate: Constants.LogTemplate)
                                                    .WriteTo.File(Constants.LogTextFile, rollingInterval: RollingInterval.Day, outputTemplate: Constants.LogTemplate)
                                                    .WriteTo.File(formatter: new JsonFormatter(renderMessage: true), path: Constants.LogJsonFile, rollingInterval: RollingInterval.Day);

                    if (options.Verbose)
                    {
                        Log.Logger = logConfig.MinimumLevel.Verbose().CreateLogger();
                    }
                    else
                    {
                        Log.Logger = logConfig.MinimumLevel.Warning().CreateLogger();
                    }
                });



            Log.Information("Started Yodel.");

            try
            {
                // Download Qt runtime if necessary
                Log.Information("Discovering or downloading Qt runtime.");
                RuntimeManager.DiscoverOrDownloadSuitableQtRuntime();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error discovering or downloading Qt runtime.");
                Environment.Exit(1);
            }

            try
            {
                // Create app directories
                Log.Information("Creating required app data directories.");
                Directory.CreateDirectory(Constants.AppDataFolder);
                Directory.CreateDirectory(Constants.LocalAppDataFolder);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Error creating required app data directories.");
                Environment.Exit(1);
            }

            // Enable support for high dpi displays
            Qml.Net.QGuiApplication.SetAttribute(ApplicationAttribute.EnableHighDpiScaling, true);

            // Set path to include local app data folder (in case we've downloaded assets)
            var pathVarSeparator = Platform.IsWindows ? ';' : ':';
            Environment.SetEnvironmentVariable("PATH", $"{Environment.GetEnvironmentVariable("PATH")}{pathVarSeparator}{Constants.LocalAppDataFolder}");
            Log.Debug("PATH set to {PATH}", Environment.GetEnvironmentVariable("PATH"));

            // OSX doesn't set current directory correctly when running applications from the Applications folder
            if (Platform.IsOSX)
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
            }

            Log.Debug("Current directory is {Directory}", Directory.GetCurrentDirectory());

            // Start application
            using (var app = new QGuiApplication(args))
            {
                using (var engine = new QQmlApplicationEngine())
                {
                    QQuickStyle.SetStyle("Material");

                    // Register interop objects
                    Qml.Net.Qml.RegisterType<YdlInterop>("YdlInterop", 1, 0);
                    Qml.Net.Qml.RegisterType<FfmpegInterop>("FfmpegInterop", 1, 0);
                    Qml.Net.Qml.RegisterType<PlatformInterop>("PlatformInterop", 1, 0);
                    Qml.Net.Qml.RegisterType<ConfigInterop>("ConfigInterop", 1, 0);
                    Qml.Net.Qml.RegisterType<LogInterop>("LogInterop", 1, 0);

                    QtDispatcher.Instance.SetApplication(app);

                    engine.Load("qml/main.qml");

                    Log.Verbose("QML engine loaded.");

#if WINDOWS
                    new Eto.Forms.Application(Eto.Platforms.WinForms).Attach();
                    Log.Verbose("Detected OS: Windows");
#elif LINUX
                    new Eto.Forms.Application(Eto.Platforms.Gtk).Attach();
                    Log.Verbose("Detected OS: Linux");
#elif OSX
                    new Eto.Forms.Application(Eto.Platforms.Mac64).Attach();
                    Log.Verbose("Detected OS: OSX");
#endif

                    try
                    {
                        return app.Exec();
                    }
                    catch (Exception ex)
                    {
                        Log.Fatal(ex, "Error executing application.");
                        return 1;
                    }
                }
            }
        }
    }
}
