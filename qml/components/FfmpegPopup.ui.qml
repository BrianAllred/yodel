import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

Popup {
    id: ffmpegPopup
    modal: true
    focus: true
    width: window.width / 2
    height: window.height / 2
    anchors.centerIn: Overlay.overlay
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

    contentItem: ColumnLayout {
        anchors.fill: parent
        spacing: 4
        anchors.margins: 8

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: "FFmpeg was not detected. It is required for transcoding certain formats. If you don't plan to transcode or don't know what this means, feel free to dismiss this dialog. Or press Download to download FFmpeg."
            wrapMode: Text.WordWrap
        }

        RowLayout {
            id: buttonRow
            Layout.fillWidth: true
            width: parent.width

            Button{
                Layout.fillWidth: true
                anchors.left: parent.left
                id: dismissFfmpegButton
                text: "Dismiss"

                Connections {
                    target: dismissFfmpegButton
                    onClicked: {
                        Ffmpeg.dismiss()
                        ffmpegPopup.close()
                    }
                }
            }

            Button{
                Layout.fillWidth: true
                anchors.right: parent.right
                id: downloadFfmpegButton
                text: "Download"

                Connections {
                    target: downloadFfmpegButton
                    onClicked: {
                        buttonRow.visible = false
                        ffmpegBusy.visible = true
                        ffmpegBusy.running = true

                        var task = Ffmpeg.download()
                        Net.await(task, function (result) {
                            ffmpegPopup.close()
                        })
                    }
                }
            }
        }

        BusyIndicator {
            id: ffmpegBusy
            Layout.fillWidth: true
            visible: false
        }
    }
}