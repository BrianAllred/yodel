import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

GroupBox {
    title: "Recode video"

    ColumnLayout {
        Label {
            text: qsTr("Set preferred video format. Video will only be recoded if necessary.")
            Layout.fillWidth: true
        }

        ComboBox {
            id: recodeFormat
            Layout.fillWidth: true
            padding: 8
            currentIndex: Ydl.options.postProcessingOptions.recodeFormat
            model: ListModel {
                id: recodeFormatList
                ListElement {
                    text: "Default"
                }
                ListElement {
                    text: "MP4"
                }
                ListElement {
                    text: "FLV"
                }
                ListElement {
                    text: "OGG"
                }
                ListElement {
                    text: "WebM"
                }
                ListElement {
                    text: "MKV"
                }
                ListElement {
                    text: "AVI"
                }
                ListElement {
                    text: "Best"
                }
                ListElement {
                    text: "Worst"
                }
            }

            Connections {
                target: recodeFormat
                onActivated: Ydl.options.postProcessingOptions.recodeFormat
                            = recodeFormat.currentIndex
            }
        }
    }
}