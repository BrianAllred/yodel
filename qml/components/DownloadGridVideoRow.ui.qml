import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

RowLayout {
    id: videoRow
    Layout.fillWidth: true
    width: parent.width
    Layout.margins: 8

    Image {
        id: thumbnailImage
        Layout.fillWidth: true
        fillMode: Image.PreserveAspectFit
        width: 200
        sourceSize.width: 200
    }

    Label {
        id: titleLabel
        Layout.fillWidth: true
        width: parent.width
        wrapMode: Text.Wrap
    }

    Label {
        id: sizeLabel
        Layout.fillWidth: true
        width: parent.width
    }

    ProgressBar {
        id: progressBar
        Layout.fillWidth: true
        width: parent.width

        from: 0
        to: 100
    }

    Label {
        id: etaLabel
        Layout.fillWidth: true
        width: parent.width
    }

    Label {
        id: downloadRateLabel
        Layout.fillWidth: true
        width: parent.width
    }

    function setInfo(video) {
        thumbnailImage.source = video.thumbnail || ""
        titleLabel.text = video.title || ""
        sizeLabel.text = video.size || ""
        progressBar.indeterminate = false
        progressBar.value = video.progress || 0
        etaLabel.text = video.eta || ""
        downloadRateLabel.text = video.rate || ""
    }
}