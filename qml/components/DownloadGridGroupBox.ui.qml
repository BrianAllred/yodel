import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

GroupBox {
    width: parent.width
    padding: 8
    Layout.margins: 8
    Layout.fillWidth: true

    property var rows

    ColumnLayout {
        id: gridColumn
        Layout.fillWidth: true
        width: parent.width
        Layout.margins: 8
        
        RowLayout {
            Layout.fillWidth: true
            width: parent.width
            Layout.margins: 8

            Label {
                Layout.fillWidth: true
                text: "Thumbnail"
            }
            Label {
                Layout.fillWidth: true
                text: "Title"
            }
            Label {
                Layout.fillWidth: true
                text: "Size"
            }
            Label {
                Layout.fillWidth: true
                text: "Progress"
            }
            Label {
                Layout.fillWidth: true
                text: "ETA"
            }
            Label {
                Layout.fillWidth: true
                text: "Speed"
            }
        }
        ToolSeparator {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.fillWidth: true
            orientation: Qt.Horizontal
        }
    }

    Component.onCompleted: {
        rows = {}

        console.log("downloadGrid: connecting download started signal")
        Ydl.downloadStartedSignal.connect(function(){
            for (var rowId in rows) {
                console.log("destroying row")
                rows[rowId].destroy()
            }

            rows = {};
        })

        console.log("downloadGrid: connecting video update signal")
        Ydl.videoUpdateSignal.connect(function(video) {
            if (!rows[video.id]) {
                var component = Qt.createComponent("DownloadGridVideoRow.ui.qml")
                rows[video.id] = component.createObject(gridColumn, {})
            }

            rows[video.id].setInfo(video)
        })
    }
}