import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

GroupBox {
    title: qsTr("Audio quality")

    ColumnLayout {
        ColumnLayout {
            RadioButton {
                id: vbrRadio
                text: qsTr("Variable bit rate (smaller file size, longer processing time)")
                checked: true

                Connections {
                    target: vbrRadio
                    onToggled: {
                        if(vbrRadio.checked) {
                            if (isNaN(Number(Ydl.options.postProcessingOptions.audioQuality))) {
                                audioQualitySlider.value = 5
                                Ydl.options.postProcessingOptions.audioQuality = 5
                            }
                            else {
                                audioQualitySlider.value = Ydl.options.postProcessingOptions.audioQuality
                            }
                        }
                    }
                }
            }
            RadioButton {
                id: cbrRadio
                text: qsTr("Constant bit rate (larger file size, faster processing time)")

                Connections {
                    target: cbrRadio
                    onToggled: {
                        if (cbrRadio.checked) {
                            if (audioQualityText.text) {
                                Ydl.options.postProcessingOptions.audioQuality = audioQualityText.text
                            }
                        }
                    }
                }
            }
        }
        ColumnLayout {
            visible: vbrRadio.checked

            CheckBox {
                id: advancedVbrCheckbox
                text: "Advanced VBR"

                Connections {
                    target: advancedVbrCheckbox
                    onClicked: {
                        if (advancedVbrCheckbox.checked) {
                            if (isNaN(Number(Ydl.options.postProcessingOptions.audioQuality))) {
                                audioQualitySlider.value = 5
                            }
                            else {
                                audioQualitySlider.value = Ydl.options.postProcessingOptions.audioQuality
                            }
                        }
                        else {
                            switch (Ydl.options.postProcessingOptions.audioQuality) {
                                case "0":
                                    bestVbrRadio.checked = true
                                    break;
                                case "9":
                                    worstVbrRadio.checked = true
                                    break;
                                case "5":
                                default:
                                    averageVbrRadio.checked = true
                                    break;
                            }
                        }
                    }
                }
            }

            RowLayout {
                id: simpleVbrRow
                visible: !advancedVbrCheckbox.checked

                RadioButton {
                    id: bestVbrRadio
                    text: "Best"

                    Connections {
                        target: bestVbrRadio
                        onToggled: {
                            if (bestVbrRadio.checked) {
                                Ydl.options.postProcessingOptions.audioQuality = 0
                            }
                        }
                    }
                }
                RadioButton {
                    id: averageVbrRadio
                    text: "Average"
                    checked: true

                    Connections {
                        target: averageVbrRadio
                        onToggled: {
                            if (averageVbrRadio.checked) {
                                Ydl.options.postProcessingOptions.audioQuality = 5
                            }
                        }
                    }
                }
                RadioButton {
                    id: worstVbrRadio
                    text: "Worst"

                    Connections {
                        target: worstVbrRadio
                        onToggled: {
                            if (worstVbrRadio.checked) {
                                Ydl.options.postProcessingOptions.audioQuality = 9
                            }
                        }
                    }
                }
            }

            RowLayout {
                id: advancedVbrRow
                visible: advancedVbrCheckbox.checked

                Label {
                    text: qsTr("Better")
                }
                Slider {
                    id: audioQualitySlider
                    from: 0
                    to: 9
                    stepSize: 1

                    Connections {
                        target: audioQualitySlider
                        onMoved: Ydl.options.postProcessingOptions.audioQuality = audioQualitySlider.value
                    }
                }
                Label {
                    text: qsTr("Worse")
                }
            }
        }
        ColumnLayout {
            visible: cbrRadio.checked

            TextField {
                id: audioQualityText
                placeholderText: qsTr("Audio quality")
                padding: 8
                Layout.margins: 8
                Layout.fillWidth: true
                hoverEnabled: true
                ToolTip.visible: hovered
                ToolTip.text: "Audio quality: specific bitrate (ie. 192K)"

                Connections {
                    target: audioQualityText
                    onTextEdited: Ydl.options.postProcessingOptions.audioQuality = audioQualityText.text
                }
            }
        }
    }



    Component.onCompleted: {
        if (isNaN(Number(Ydl.options.postProcessingOptions.audioQuality))) {
            cbrRadio.checked = true
            audioQualityText.text = Ydl.options.postProcessingOptions.audioQuality
        }
        else {
            vbrRadio.checked = true
            audioQualitySlider.value = parseInt(Ydl.options.postProcessingOptions.audioQuality)
        }
    }
}