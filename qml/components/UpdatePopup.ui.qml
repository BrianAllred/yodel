import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

Popup {
    id: updatePopup
    modal: true
    focus: true
    width: window.width / 2
    height: window.height / 2
    anchors.centerIn: Overlay.overlay
    closePolicy: Popup.NoAutoClose

    contentItem: ColumnLayout {
        anchors.fill: parent
        spacing: 4
        anchors.margins: 8

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: "Updating internals. Please wait..."
            wrapMode: Text.WordWrap
        }

        BusyIndicator {
            id: updateBusy
            Layout.fillWidth: true
            visible: true
        }
    }

    Component.onCompleted: {
        var task = Ydl.update()
        Net.await(task, function (result) {
            updatePopup.close()
        })
    }
}