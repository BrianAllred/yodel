import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

Popup {
    id: ydlPopup
    modal: true
    focus: true
    width: window.width / 2
    height: window.height / 2
    anchors.centerIn: Overlay.overlay
    closePolicy: Popup.NoAutoClose

    property var pythonOnPath: false

    contentItem: ColumnLayout {
        anchors.fill: parent
        spacing: 4
        anchors.margins: 8

        Label {
            id: ydlLabel
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            text: "youtube-dl not detected. Click Download below or close Yodel and install it manually."
            wrapMode: Text.WordWrap
        }

        Text {
            id: ydlLink
            textFormat: Text.RichText
            anchors.horizontalCenter: parent.horizontalCenter
            text: '<html><style type="text/css"></style><a href="https://rg3.github.io/youtube-dl/">https://rg3.github.io/youtube-dl/</a></html>'
            onLinkActivated: Qt.openUrlExternally(link)
        }

        Button{
            Layout.fillWidth: true
            anchors.left: parent.left
            id: downloadYdlButton
            text: "Download"

            Connections {
                target: downloadYdlButton
                onClicked: {
                    if (ydlPopup.pythonOnPath) {
                        downloadYdlButton.visible = false
                        ydlBusy.visible = true
                        ydlBusy.running = true

                        var task = Ydl.downloadYdl()
                        Net.await(task, function (result) {
                            ydlPopup.close()
                        })
                    }
                    else {
                        Platform.exitApplication()
                    }
                }
            }
        }

        BusyIndicator {
            id: ydlBusy
            Layout.fillWidth: true
            visible: false
        }
    }

    Component.onCompleted: {
        ydlPopup.pythonOnPath = Ydl.pythonOnPath()

        if (Platform.isLinux() && !ydlPopup.pythonOnPath)
        {
            ydlLabel.text = "Python is not detected and is required to function. Please install Python and try again."
            downloadYdlButton.text = "Close"
        }
    }    
}