import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

GroupBox {
    title: "Download format"

    ColumnLayout {
        Label {
            text: qsTr("Set preferred video format. This format will be downloaded if available.")
            Layout.fillWidth: true
        }

        ComboBox {
            id: videoFormat
            Layout.fillWidth: true
            padding: 8
            currentIndex: Ydl.options.videoFormatOptions.format
            model: ListModel {
                id: videoFormatList
                ListElement {
                    text: "Default"
                }
                ListElement {
                    text: "MP4"
                }
                ListElement {
                    text: "FLV"
                }
                ListElement {
                    text: "OGG"
                }
                ListElement {
                    text: "WebM"
                }
                ListElement {
                    text: "MKV"
                }
                ListElement {
                    text: "AVI"
                }
                ListElement {
                    text: "Best"
                }
                ListElement {
                    text: "Worst"
                }
            }

            Connections {
                target: videoFormat
                onActivated: Ydl.options.videoFormatOptions.format = videoFormat.currentIndex
            }
        }
    }
}