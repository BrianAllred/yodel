import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

GroupBox {
    title: qsTr("Audio format")

    ComboBox {
        id: audioFormat
        padding: 8
        currentIndex: Ydl.options.postProcessingOptions.audioFormat
        model: ListModel {
            id: audioFormatList
            ListElement {
                text: "Best"
            }
            ListElement {
                text: "3GP"
            }
            ListElement {
                text: "AAC"
            }
            ListElement {
                text: "Vorbis"
            }
            ListElement {
                text: "MP3"
            }
            ListElement {
                text: "M4A"
            }
            ListElement {
                text: "Opus"
            }
            ListElement {
                text: "WAV"
            }
        }

        Connections {
            target: audioFormat
            onActivated: Ydl.options.postProcessingOptions.audioFormat
                            = audioFormat.currentIndex
        }
    }
}