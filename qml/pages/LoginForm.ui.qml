import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

Page {
    id: page

    GridLayout {
        id: gridLayout
        columns: 1
        anchors.fill: parent

        GroupBox {
            title: "Login"
            Layout.fillWidth: true
            width: parent.width
            Layout.margins: 8

            ColumnLayout {
                Layout.fillWidth: true
                width: parent.width

                TextField {
                    Layout.fillWidth: true
                    width: parent.width
                    id: username
                    placeholderText: qsTr("Username")
                    padding: 8
                    text: Ydl.options.authenticationOptions.username

                    Connections {
                        target: username
                        onTextEdited: Ydl.options.authenticationOptions.username = username.text
                    }
                }

                TextField {
                    Layout.fillWidth: true
                    width: parent.width
                    id: password
                    placeholderText: qsTr("Password")
                    padding: 8
                    text: Ydl.options.authenticationOptions.password
                    echoMode: TextInput.Password

                    Connections {
                        target: password
                        onTextEdited: Ydl.options.authenticationOptions.password = password.text
                    }
                }    

                TextField {
                    Layout.fillWidth: true
                    width: parent.width
                    id: twoFactor
                    placeholderText: qsTr("2FA")
                    padding: 8
                    text: Ydl.options.authenticationOptions.twoFactor

                    Connections {
                        target: twoFactor
                        onTextEdited: Ydl.options.authenticationOptions.twoFactor = twoFactor.text
                    }
                }
            }
        } 

        GroupBox {       
            title: "Video password"
            Layout.fillWidth: true
            width: parent.width
            Layout.margins: 8

            ColumnLayout {
                CheckBox {
                    Layout.fillWidth: true
                    width: parent.width
                    id: videoPasswordCheck
                    text: "Is the video password protected?"
                }

                TextField {
                    Layout.fillWidth: true
                    width: parent.width
                    id: videoPassword
                    placeholderText: qsTr("Video password")
                    padding: 8
                    text: Ydl.options.authenticationOptions.videoPassword
                    echoMode: TextInput.Password
                    visible: videoPasswordCheck.checked

                    Connections {
                        target: videoPassword
                        onTextEdited: Ydl.options.authenticationOptions.videoPassword = videoPassword.text
                    }
                }
            }
        }

        Item { Layout.fillHeight: true }
    }
}