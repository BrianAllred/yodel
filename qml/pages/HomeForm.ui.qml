import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.0
import "../interop"
import "../components"

Page {
    id: page
    anchors.fill: parent
    title: qsTr("Download")
    property alias buttonDownload: buttonDownload
    property alias urlsText: urlsText
    property alias locationText: locationText

    ColumnLayout {
        anchors.fill: parent
        spacing: 4
        anchors.margins: 8

        RowLayout {
            TextArea {
                id: urlsText
                width: parent.width
                padding: 8
                Layout.margins: 8
                Layout.fillWidth: true
                placeholderText: "URLs"
                selectByMouse: true
            }

            Button {
                id: urlsInfoButton
                text: "info"
                flat: true

                Connections {
                    target: urlsInfoButton
                    onClicked: {
                        urlsToolTip.visible = true
                        sleepTimer.start()
                    }
                }

                ToolTip {
                    id: urlsToolTip
                    visible: false
                    text: "URLs\n\nMultiple lines and playlists allowed."
                }

                Timer {
                    id: sleepTimer
                    running: false
                    repeat: false

                    onTriggered: urlsToolTip.visible = false
                }
            }
        }

        RowLayout {
            TextField {
                id: locationText
                width: parent.width
                padding: 8
                Layout.margins: 8
                Layout.fillWidth: true
                placeholderText: "Download location"
                selectByMouse: true

                Connections {
                    target: locationText
                    onTextEdited: Ydl.options.filesystemOptions.output = locationText.text
                }
            }

            Button {
                id: downloadLocationButton
                text: "select folder"
                flat: true

                Connections {
                    target: downloadLocationButton
                    onClicked: {
                        if (Platform.isWindows()) {
                            var fileName = locationText.text.substring(locationText.text.lastIndexOf('\\'))
                            var folder = locationText.text.substring(0, locationText.text.lastIndexOf('\\'))
                        } else {
                            var fileName = locationText.text.substring(locationText.text.lastIndexOf('/'))
                            var folder = locationText.text.substring(0, locationText.text.lastIndexOf('/'))
                        }

                        var newFolder = Platform.openFolderDialog(folder);
                        locationText.text = newFolder + fileName;
                        Ydl.options.filesystemOptions.output = locationText.text
                    }
                }
            }
        }

        Button {
            id: buttonDownload
            anchors.horizontalCenter: parent.horizontalCenter
            text: "DOWNLOAD"

            Connections {
                target: buttonDownload
                onClicked: {
                    buttonDownload.text = "DOWNLOADING..."
                    buttonDownload.enabled = false

                    if (locationText.text.length > 0) {
                        Ydl.options.filesystemOptions.output = locationText.text
                    }

                    var task = Ydl.download(urlsText.text.replace(/\n/g, " "))
                    Net.await(task, function (result) {
                        buttonDownload.text = "DOWNLOAD"
                        buttonDownload.enabled = true
                    })
                }
            }
        }

        ToolSeparator {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            orientation: Qt.Horizontal
        }

        DownloadGridGroupBox {
            id: downloadGrid
        }

        Item { Layout.fillHeight: true }
    }

    Timer {
        id: locationTextTimer
        running: false
        repeat: false

        onTriggered: {
            if (!!Ydl.options.filesystemOptions.output) {
                Log.verbose("setting location text to: " + Ydl.options.filesystemOptions.output)
                locationText.text = Ydl.options.filesystemOptions.output
            } else {
                Log.verbose("setting location text to: " + Platform.getDefaultOutput())
                locationText.text = Platform.getDefaultOutput()
            }

            locationText.enabled = true
        }
    }

    Component.onCompleted: {
        locationText.enabled = false
        locationTextTimer.start()
    }
}