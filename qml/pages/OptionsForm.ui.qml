import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../optionsTabs"

Page {
    anchors.fill: parent
    title: qsTr("Options")

    ColumnLayout {
        anchors.fill: parent

        TabBar {
            id: tabBar
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            Layout.fillWidth: true

            TabButton {
                text: qsTr("Basic")
            }
            TabButton {
                text: qsTr("Audio")
            }
            TabButton {
                text: qsTr("Video")
            }
        }

        StackLayout {
            currentIndex: tabBar.currentIndex
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            Layout.fillWidth: true

            BasicOptionsTabForm {}
            AudioOptionsTabForm {}
            VideoOptionsTabForm {}
        }
    }
}
