pragma Singleton
import QtQuick 2.12
import LogInterop 1.0

Item {
    function verbose(message) {
        interop.verbose(message)
    }

    function information(message) {
        interop.information(message)
    }

    function debug(message) {
        interop.debug(message)
    }

    function error(message) {
        interop.error(message)
    }

    function fatal(message) {
        interop.fatal(message)
    }

    LogInterop {
        id: interop
    }
}