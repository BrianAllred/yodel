pragma Singleton
import QtQuick 2.12
import YdlInterop 1.0

Item {
    id: ydl
    property var options

    signal videoUpdateSignal(var videoInfoModel)
    signal downloadStartedSignal()

    function constructor() {
        options = Config.loadConfig()
        configSyncTimer.start()
    }

    function download(urls) {
        Config.saveConfig(options)

        ydl.downloadStartedSignal()
        urls = urls.replace(/\n/g, " ")
        var client = interop.newClient()
        client.options = options
        client.retrieveAllInfo = true
        client.videoUrl = urls
        return interop.download(client)
    }

    function update() {
        return interop.update()
    }

    function onPath() {
        return interop.onPath()
    }

    function pythonOnPath() {
        return interop.pythonOnPath()
    }

    function downloadYdl() {
        return interop.downloadYdl()
    }

    Timer {
        id: configSyncTimer
        running: false
        repeat: true

        onTriggered: {
            Config.syncConfig(options)
        }
    }

    YdlInterop {
        id: interop
    }

    Component.onCompleted: {
        interop.videoUpdateSignal.connect(function(video) {
            ydl.videoUpdateSignal(video)
        })
    }
}
