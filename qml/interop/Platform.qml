pragma Singleton
import QtQuick 2.12
import PlatformInterop 1.0

Item {
    function isWindows() {
        return interop.isWindows
    }

    function isOSX() {
        return interop.isOSX
    }

    function isLinux() {
        return interop.isLinux
    }

    function exitApplication() {
        interop.exitApplication()
    }

    function openFolderDialog(directory) {
        return interop.openFolderDialog(directory);
    }

    function getDefaultOutput() {
        return interop.getDefaultOutput()
    }

    PlatformInterop {
        id: interop
    }
}