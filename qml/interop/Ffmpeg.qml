pragma Singleton
import QtQuick 2.12
import FfmpegInterop 1.0

Item {
    function onPath() {
        return interop.onPath()
    }

    function dismiss()
    {
        interop.dismiss()
    }

    function download()
    {
        return interop.download()
    }

    FfmpegInterop {
        id: interop
    }
}