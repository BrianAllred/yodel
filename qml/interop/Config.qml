pragma Singleton
import QtQuick 2.12
import ConfigInterop 1.0

Item {
    function saveConfig(options) {
        interop.saveConfig(options)
    }

    function loadConfig() {
        return interop.loadConfig();
    }

    function syncConfig(options) {
        interop.syncConfig(options)
    }

    ConfigInterop {
        id: interop
    }
}