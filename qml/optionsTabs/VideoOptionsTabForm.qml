import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"
import "../components"

Page {
    id: page

    GridLayout {
        id: gridLayout
        columns: 1
        rowSpacing: 10
        anchors.fill: parent

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "NOTE: Yodel attempts to download the best quality video by default.\n\nOnly change these options if you require a *specific* video format."
        }

        ToolSeparator {
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            padding: 8
            Layout.margins: 8
            Layout.fillWidth: true
            orientation: Qt.Horizontal
        }

        VideoFormatGroupBox {}

        VideoRecodeGroupBox {}

        Item { Layout.fillHeight: true }
    }
}