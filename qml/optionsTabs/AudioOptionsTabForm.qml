import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"
import "../components"

Page {
    id: page

    GridLayout {
        id: gridLayout
        columns: 1
        anchors.fill: parent

        AudioQualityGroupBox {}

        AudioFormatGroupBox {}

        Item { Layout.fillHeight: true }
    }
}