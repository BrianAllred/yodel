import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "../interop"

Page {
    id: page

    GridLayout {
        id: gridLayout
        columns: 1
        anchors.fill: parent

        GroupBox {
            title: "Abort on error"

            CheckBox {
                id: abortOnError
                text: qsTr("Abort the download when encountering any error (not just fatal ones).\nIf unsure, leave unchecked.")
                Layout.fillWidth: true
                checked: Ydl.options.generalOptions.abortOnError

                Connections {
                    target: abortOnError
                    onClicked: {
                        Ydl.options.generalOptions.abortOnError = abortOnError.checked
                    }
                }
            }
        }

        GroupBox {
            title: "Ignore errors"

            CheckBox {
                id: ignoreErrors
                text: qsTr("Continue downloading regardless of errors. Useful, for example, if downloading a playlist with deleted videos.\nIf unsure, leave unchecked.")
                Layout.fillWidth: true
                checked: Ydl.options.generalOptions.ignoreErrors

                Connections {
                    target: ignoreErrors
                    onClicked: {
                        Ydl.options.generalOptions.ignoreErrors = ignoreErrors.checked
                    }
                }
            }
        }

        GroupBox {
            title: "Extract audio"

            RowLayout {
                CheckBox {
                    id: extractAudio
                    text: qsTr("Download only the audio, discarding the video.")
                    Layout.fillWidth: true
                    checked: Ydl.options.postProcessingOptions.extractAudio

                    Connections {
                        target: extractAudio
                        onClicked: { 
                            Ydl.options.postProcessingOptions.extractAudio = extractAudio.checked 
                            if (!extractAudio.checked) {
                                keepVideo.checked = false
                            }
                        }
                    }
                }

                CheckBox {
                    id: keepVideo
                    text: qsTr("Don't discard video (keep both audio and video files).")
                    Layout.fillWidth: true
                    checked: Ydl.options.postProcessingOptions.keepVideo
                    enabled: extractAudio.checked
                    visible: extractAudio.checked

                    Connections {
                        target: keepVideo
                        onClicked: Ydl.options.postProcessingOptions.keepVideo = keepVideo.checked
                    }
                }
            }
        }

        Item { Layout.fillHeight: true }
    }
}