import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import "interop"
import "components"

ApplicationWindow {
    id: window
    visible: true
    width: 1024
    height: 768
    title: qsTr("Yodel")

    Material.theme: Material.Dark

    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: homeForm.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: {
                if (homeForm.depth > 1) {
                    homeForm.pop()
                } else {
                    drawer.open()
                }
            }
        }

        Label {
            text: homeForm.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height

        Column {
            anchors.fill: parent

            ItemDelegate {
                text: qsTr("Options")
                width: parent.width
                onClicked: {
                    homeForm.push("pages/OptionsForm.ui.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Login")
                width: parent.width
                onClicked: {
                    homeForm.push("pages/LoginForm.ui.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: homeForm
        initialItem: "pages/HomeForm.ui.qml"
        anchors.fill: parent
    }

    FfmpegPopup {
        id: ffmpegPopup
    }

    YdlDownloadPopup {
        id: ydlPopup
    }

    UpdatePopup {
        id: updatePopup
    }

    Component.onCompleted: {
        Ydl.constructor()
        
        if(!Ffmpeg.onPath())
        {
            ffmpegPopup.open()
        }

        if(!Ydl.onPath() || !Ydl.pythonOnPath())
        {
            ydlPopup.open()
        }
        else 
        {
            updatePopup.open()
        }
    }
}
