#define use_msiproduct
#define use_vc2010
#define use_vc2015

#define MyAppName 'Yodel'
#define MyAppSetupName 'Yodel Setup'
#define MyAppVersion '0.5.0'
#define MyAppURL 'https://gitlab.com/BrianAllred/yodel'

[Setup]
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher=Brian Allred
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
WizardStyle=modern
ArchitecturesAllowed=x64
Compression=lzma2
SolidCompression=yes
ArchitecturesInstallIn64BitMode=x64
DefaultDirName={autopf}\{#MyAppName}
DefaultGroupName={#MyAppName}
PrivilegesRequired=admin
OutputBaseFilename={#MyAppSetupName}-{#MyAppVersion}
DisableReadyPage=no
DisableReadyMemo=no

; supported languages
#include "scripts\lang\english.iss"
#include "scripts\lang\german.iss"
#include "scripts\lang\french.iss"
#include "scripts\lang\italian.iss"
#include "scripts\lang\dutch.iss"

#ifdef UNICODE
#include "scripts\lang\chinese.iss"
#include "scripts\lang\polish.iss"
#include "scripts\lang\russian.iss"
#include "scripts\lang\japanese.iss"
#endif

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"

[InstallDelete]
Type: filesandordirs; Name: "{app}\qml"

[UninstallDelete]
Type: filesandordirs; Name: "{app}\cache"

[Files]
Source: "publish\*"; Flags: recursesubdirs; DestDir: {app}; Excludes: "*.pdb"

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppName}.exe"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppName}.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppName}.exe"; Description: "{cm:LaunchProgram,{#MyAppName}}"; Flags: nowait postinstall skipifsilent

[CustomMessages]
DependenciesDir=MyProgramDependencies
WindowsServicePack=Windows %1 Service Pack %2

#include "scripts\products.iss"
#include "scripts\products\stringversion.iss"
#include "scripts\products\winversion.iss"
#include "scripts\products\fileversion.iss"
#ifdef use_msiproduct
#include "scripts\products\msiproduct.iss"
#endif
#ifdef use_vc2010
#include "scripts\products\vcredist2010.iss"
#endif
#ifdef use_vc2015
#include "scripts\products\vcredist2015.iss"
#endif

[Code]
function InitializeSetup(): boolean;
begin
	// initialize windows version
	initwinversion();

#ifdef use_vc2010
	vcredist2010('10'); // min allowed version is 10.0
#endif

#ifdef use_vc2015
	vcredist2015('14'); // install if version < 14.0
#endif

	Result := true;
end;
